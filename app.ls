require! {
  'koa'
  'koa-logger': logger
  'koa-static': serve
  'koa-bodyparser': bodyParser
  'koa-jade': jade
  'lodash': _
  'fs'
  #'async'
  'co-multipart': multipart
}

app = koa!

router = require('koa-router')!

app.use router.routes!
app.use router.allowedMethods!

app.port = "4000"

app.use jade.middleware do
  viewPath: __dirname + '/views'
  debug: true
  compileDebug: true

app.use logger!
app.use serve 'public'

app.use (next) ->*
  @render 'index',
    title: 'uploader test'

router.post '/', (next) ->*
  parts = yield multipart(this)

  files = []

  _.each parts.files, (file) ->
    fs.rename file.path, '/data/files/' + file.filename, (err) ->
      if err
        throw err
    files.push('https://assets.auggod.io/' + file.filename)
  
  parts.dispose!

  @body = files: files
  

app.use (next) ->*
  yield next
  if 404 != @status
    return
  # we need to explicitly set 404 here
  # so that koa doesn't assign 200 on body=
  @status = 404
  switch @accepts('html', 'json')
    when 'html'
      @type = 'html'
      @body = '<p>Page Not Found</p>'
    when 'json'
      @body = message: 'Page Not Found'
    else
      @type = 'text'
      @body = 'Page Not Found'

app.listen app.port, ->
  console.log 'Uploader has started', app.port
